package com.iot.app.springboot.vo;

import java.io.Serializable;
import java.util.List;

import com.iot.app.springboot.dao.entity.HeatMapData;
import com.iot.app.springboot.dao.entity.POITrafficData;
import com.iot.app.springboot.dao.entity.TotalTrafficData;
import com.iot.app.springboot.dao.entity.WindowTrafficData;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Response object containing traffic details that will be sent to dashboard.
 *
 * @author Varadharajan
 */
@Getter
@Setter
@ToString
public class Response implements Serializable {
    private List<TotalTrafficData> totalTraffic;
    private List<WindowTrafficData> windowTraffic;
    private List<POITrafficData> poiTraffic;
    private List<HeatMapData> heatMap;
}
