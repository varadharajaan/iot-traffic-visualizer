package com.iot.app.springboot.dao.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;
import java.util.Date;

/**
 * Heatmap data entity
 * @author Varadharajan
 */
@Getter
@Setter
@Table("heat_map")
public class HeatMapData implements Serializable {
    @PrimaryKeyColumn(name = "latitude",ordinal = 0,type = PrimaryKeyType.PARTITIONED)
    private double latitude;
    @PrimaryKeyColumn(name = "longitude",ordinal = 1,type = PrimaryKeyType.CLUSTERED)
    private double longitude;
    @Column(value = "totalcount")
    private int totalCount;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone="MST")
    @Column(value = "timestamp")
    private Date timeStamp;

    public HeatMapData(double latitude, double longitude, int totalCount, Date timeStamp) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.totalCount = totalCount;
        this.timeStamp = timeStamp;
    }

}
