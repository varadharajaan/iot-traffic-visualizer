package com.iot.app.spark.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Coordinate data transfer object
 *
 * @author Varadharajan
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
public class Coordinate implements Serializable {

    private double latitude;

    private double longitude;

    public Coordinate(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }


}
