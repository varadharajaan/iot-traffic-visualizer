package com.iot.app.spark.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Class to represent attributes of POI
 * 
 * @author Varadharajan
 *
 */
@Getter
@Setter
public class POIData implements Serializable {
	private double latitude;
	private double longitude;
	private double radius;

}
