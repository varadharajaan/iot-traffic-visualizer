package com.iot.app.spark.entity;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

/**
 * Class to represent poi_tarffic db table
 * 
 * @author Varadharajan
 *
 */
@Getter
@Setter
public class POITrafficData implements Serializable {

	private String vehicleId;
	private double distance;
	private String vehicleType;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone="MST")
	private Date timeStamp;

}
