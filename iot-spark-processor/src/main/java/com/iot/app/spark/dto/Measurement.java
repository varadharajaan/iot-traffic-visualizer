package com.iot.app.spark.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Measurement entity
 *
 * @author Varadharajan
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class Measurement implements Serializable {

    private Coordinate coordinate;

    private Coordinate roundedCoordinate;

    private Date timestamp;

    public Measurement(Coordinate coordinate, Date timestamp) {
        this.coordinate = coordinate;
        this.timestamp = timestamp;
    }

}
