package com.iot.app.spark.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

/**
 * Class to represent window_tarffic db table
 *
 * @author Varadharajan
 */
@Getter
@Setter
public class WindowTrafficData implements Serializable {

    private String routeId;
    private String vehicleType;
    private long totalCount;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "MST")
    private Date timeStamp;
    private String recordDate;

}
