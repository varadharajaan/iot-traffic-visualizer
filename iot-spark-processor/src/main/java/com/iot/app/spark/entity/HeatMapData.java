package com.iot.app.spark.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Heatmap data entity
 *
 * @author Varadharajan
 */
@Getter
@Setter
public class HeatMapData implements Serializable {

    private double latitude;
    private double longitude;
    private int totalCount;
    private Date timeStamp;

    public HeatMapData(double latitude, double longitude, int count, Date timeStamp) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.totalCount = count;
        this.timeStamp = timeStamp;
    }

}
